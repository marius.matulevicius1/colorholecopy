﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveHoleToEnd : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private float centerPosition;
    [SerializeField]
    private float endPosition;

    private bool inCenter = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveToNextLevel()
    {
        StartCoroutine(moveToCenter());
        GameManager.OpenWall();
    }

    private IEnumerator moveToCenter()
    {
        if (transform.position.x > centerPosition)
            while (transform.position.x > centerPosition)
            {
                transform.Translate(Vector3.left * 2 * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }

        if (transform.position.x < centerPosition)
            while (transform.position.x < centerPosition)
            {
                transform.Translate(Vector3.right * Time.deltaTime * 2);
                yield return new WaitForEndOfFrame();
            }
        StartCoroutine(MoveToEnd());
        GameObject.Find("CameraHolder").GetComponent<moveCameraToEnd>().moveCamera();
    }

    IEnumerator MoveToEnd()
    {
        while(transform.position.z < endPosition)
        {
            transform.Translate(Vector3.forward * 2 * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }
        if (transform.position.z >= endPosition)
        {
            var restartButton = GameObject.Find("RestartButton");
            restartButton.GetComponent<Image>().enabled = true;
            restartButton.GetComponentInChildren<Text>().enabled = true;
        }
    }
}
