﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControlls : MonoBehaviour
{
    private float deltaX, deltaY;
    private Rigidbody rb;
    private float touchPosStartX, touchPosStartY;
    private float lastMovedX = 0, lastMovedY = 0;
    public float speed = 20;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.gameOver)
        {
            return;
        }
        if (GameManager.instance.CollectedAll)
        {
            return;
        }
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            Vector2 touchPos = Camera.main.ScreenToViewportPoint(touch.position);

            

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    deltaX = touchPos.x - transform.position.x;
                    deltaY = touchPos.y - transform.position.y;
                    touchPosStartX = touchPos.x;
                    touchPosStartY = touchPos.y;

                    break;
                case TouchPhase.Moved:
                    Vector3 direction = new Vector3(touchPos.x - touchPosStartX, 0, touchPos.y - touchPosStartY);
                    if (lastMovedX != 0 && lastMovedY != 0)
                    {
                        direction = new Vector3(touchPos.x - lastMovedX, 0, touchPos.y - lastMovedY);
                        lastMovedX = touchPos.x;
                        lastMovedY = touchPos.y;
                    }
                    transform.Translate(direction * speed * Time.deltaTime);
                    fixateMovement();
                    lastMovedX = touchPos.x;
                    lastMovedY = touchPos.y;

                    break;
                case TouchPhase.Ended:
                    rb.velocity = Vector3.zero;
                    lastMovedX = 0;
                    lastMovedY = 0;
                    break;
                    
            }
        }
    }

    private void fixateMovement()
    {
        float posX = Mathf.Clamp(transform.position.x, 26.50f, 32f);
        float posZ = Mathf.Clamp(transform.position.z, 8.7f, 27f);

        transform.position = new Vector3(posX, transform.position.y, posZ);
    }
}
