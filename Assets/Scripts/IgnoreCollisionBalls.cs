﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollisionBalls : MonoBehaviour
{
    Collider plane;

    void Start()
    {
        plane = GameObject.FindGameObjectWithTag("plane2").GetComponent<BoxCollider>();
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "hole")
        {
            Debug.Log("Collisoion");
            Physics.IgnoreCollision(GetComponent<Collider>(), plane);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "hole")
        {
            Debug.Log("NoCollisoion");
            Physics.IgnoreCollision(GetComponent<Collider>(), plane, false);
        }
    }
}
