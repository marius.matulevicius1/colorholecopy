﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour
{
    // Start is called before the first frame update
    public CameraShake cameraShake; 

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "DestroyableLevel1")
        {
            Destroy(col.gameObject);
            Vibration.Vibrate(100);
            GameManager.instance.updateSlider();

        }
        if (col.gameObject.tag == "DestroyableLevel2")
        {
            Destroy(col.gameObject);
            Vibration.Vibrate(100);
        }
        if (col.gameObject.tag == "BadObject")
        {
            Destroy(col.gameObject);
            GameManager.GameOver();
            Vibration.Vibrate(100);

            StartCoroutine(cameraShake.Shake(.15f, .6f));
        }
    }
}
