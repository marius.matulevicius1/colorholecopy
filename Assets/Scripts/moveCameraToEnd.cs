﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCameraToEnd : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private float endPos;

    public void moveCamera()
    {
        StartCoroutine(moveToEnd());
    }

    IEnumerator moveToEnd()
    {
        while(transform.position.z < endPos)
        {
            transform.Translate(Vector3.forward * 3 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
            if (transform.position.z == endPos)
            {
                GameManager.GameOver();
            }
        }
    }
}
