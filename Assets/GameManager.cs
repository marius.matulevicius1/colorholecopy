﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;

    public bool CollectedAll = false;

    public static bool gameOver = false;

    int destroyableCount = 0;
    public Slider slider;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }



    void Start()
    {
        var destroyables = GameObject.FindGameObjectsWithTag("DestroyableLevel1");
        destroyableCount = destroyables.Length;
        slider.maxValue = destroyableCount;
    }

    public static void GameOver()
    {
        var gameOverText = GameObject.Find("GameOverText");
        var restartButton = GameObject.Find("RestartButton");

        gameOverText.GetComponent<Text>().enabled = true;
        restartButton.GetComponent<Image>().enabled = true;
        restartButton.GetComponentInChildren<Text>().enabled = true;


        Vibration.Vibrate(60);
        gameOver = true;
    }

    public void restartGame()
    {
        SceneManager.LoadScene(0);
        gameOver = false;
    }


    public static void OpenWall()
    {
        var theWall = GameObject.Find("ExitWall");
        theWall.GetComponent<Animator>().SetTrigger("Animate");
    }

    public void updateSlider()
    {

        if (slider.value == slider.maxValue) {
            var theHole = GameObject.Find("TheHole");
            theHole.GetComponent<MoveHoleToEnd>().MoveToNextLevel();
            CollectedAll = true;
            theHole.GetComponent<disableAllChildren>().disable();
        }
        slider.value += 1;

    }


}
